//
//  FourthViewController.swift
//  myFirstApp
//
//  Created by Edward Chandra on 24/04/19.
//  Copyright © 2019 Edward Chandra. All rights reserved.
//

import UIKit

class FourthViewController: UIViewController {

    var date: String = ""
    
    @IBOutlet weak var dateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(date)
        
        dateLabel.text = "\(date)"
        

        // Do any additional setup after loading the view.
    }
    
    
    

    

}
