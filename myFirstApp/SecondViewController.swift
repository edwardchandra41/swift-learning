//
//  SecondViewController.swift
//  myFirstApp
//
//  Created by Edward Chandra on 24/04/19.
//  Copyright © 2019 Edward Chandra. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var buttonDesign: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        buttonDesign.cornerRadius()
    }
}
