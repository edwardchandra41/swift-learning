//
//  ThirdViewController.swift
//  myFirstApp
//
//  Created by Edward Chandra on 24/04/19.
//  Copyright © 2019 Edward Chandra. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {
    
    @IBOutlet weak var buttonImage: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        buttonDesign()
        
    }
    
    func buttonDesign(){
       //Design button
        buttonImage.imageView?.contentMode = .scaleAspectFit
        
        buttonImage.layer.cornerRadius = 12
        buttonImage.layer.masksToBounds = true
    }
    


}
