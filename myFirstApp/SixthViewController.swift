//
//  SixthViewController.swift
//  myFirstApp
//
//  Created by Edward Chandra on 24/04/19.
//  Copyright © 2019 Edward Chandra. All rights reserved.
//

import UIKit

class SixthViewController: UIViewController {

   
    
    @IBOutlet weak var weightLabel: UILabel!
    
    @IBOutlet weak var heightLabel: UILabel!
    
    @IBOutlet weak var bmiLabel: UILabel!
    
    var weight: Float = 0.0
    var height: Float = 0.0
    
    @IBOutlet weak var weightLabelOutlet: UISlider!
    
    @IBOutlet weak var heightLabelOutlet: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        weight = weightLabelOutlet.value
        height = heightLabelOutlet.value
        
        weightLabel.text = "\(weight) kg"
        heightLabel.text = "\(height) m"
        
        
        
        bmiLabel.text = "\(String(format: "%.1f", Float(weight) * Float(height)))"
        
        
    }
    

    @IBAction func weightSliderAct(_ sender: UISlider) {
        
        weightLabel.text = "\(String(format: "%.1f", Float(sender.value))) kg"
        
        weight = Float(sender.value)
        
        
    }
    
    @IBAction func heightSliderAct(_ sender: UISlider) {
        
        heightLabel.text = "\(String(format: "%.1f", Float(sender.value))) m"
        
        height = Float(sender.value)
        
    }
    
    
    
}
