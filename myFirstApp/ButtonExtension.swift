//
//  Button.swift
//  myFirstApp
//
//  Created by Edward Chandra on 05/01/20.
//  Copyright © 2020 Edward Chandra. All rights reserved.
//

import UIKit

extension UIButton{
    
    func cornerRadius(){
        self.layer.cornerRadius = 10
    }
    
}
